function binarySearch(list, key) {
  first = 0;
  last = list.length - 1;
  while (first <= last) {
    mid = Math.floor((first + last) / 2);
    if (key === list[mid]) {

      return mid;

    } else if (list[mid] < key) {
      first = mid + 1;

    } else {
      last = mid - 1;
    }
  }
  return -1;
}
console.log(binarySearch([1, 2, 3, 4, 5, 6], 3))